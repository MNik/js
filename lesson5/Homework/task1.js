/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

document.addEventListener('DOMContentLoaded', function(){

  let commentPrototyp = {
    avatarUrl: 'https://cdn2.iconfinder.com/data/icons/happy-smile-1/512/16_smiley-48.png',
    likes: 0,
    likePlus: function(){
      this.likes += 1;
    }
  };

  comment.prototype = commentPrototyp;

  function comment(name, text, avatarUrl) {
    this.name = name;
    this.text = text;
    this.avatarUrl = avatarUrl;
    this.likes = 0;
    
    if( avatarUrl === undefined){
      this.avatarUrl = commentPrototyp.avatarUrl;
    }     
  }

  let myComment1 = new comment('Michale', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem minima, ullam et earum iusto, velit odit dolorem voluptas ducimus vero eaque aspernatur expedita recusandae distinctio molestiae beatae modi eius tempore.', 'https://cdn2.iconfinder.com/data/icons/happy-smile-1/512/32_smiley-48.png'),
  myComment2 = new comment('Jack', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos illo reprehenderit libero voluptates, corrupti est, officia aspernatur suscipit. Molestiae optio totam quae itaque doloribus praesentium iure error nihil, magni quaerat.', 'https://cdn2.iconfinder.com/data/icons/happy-smile-1/512/11_smiley-48.png'),
  myComment3 = new comment('Jane', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt debitis aut, libero dicta ipsa, aliquid enim, iure quaerat est inventore ducimus mollitia harum consequuntur at eveniet totam quisquam accusamus dolor.'),
  myComment4 = new comment('David', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita nihil culpa cumque aperiam vero quia, omnis ipsam tempore impedit deleniti illum fugiat sed obcaecati excepturi minima cum. Debitis, blanditiis aliquid!', 'https://cdn2.iconfinder.com/data/icons/happy-smile-1/512/51_smiley-48.png'),
  CommentsArray = [myComment1, myComment2, myComment3, myComment4];

  function release(arr){

    arr.forEach(function(obj){
      let CommentsFeed = document.getElementById('CommentsFeed'),
      message = document.createElement('div'),
      p1 = document.createElement('p'),
      p2 = document.createElement('p'),
      p3 = document.createElement('p'),
      spanAvatar = document.createElement('span'),
      spanName = document.createElement('span'),
      avatar = document.createElement('img'),
      spanLike = document.createElement('span'),
      like = document.createElement('button');

      avatar.src = obj.avatarUrl;
      spanAvatar.appendChild(avatar);
      p1.appendChild(spanAvatar);

      spanName.innerText = obj.name;
      spanName.id = 'name';
      p1.appendChild(spanName);

      message.appendChild(p1);

      p2.innerText = obj.text;
      message.appendChild(p2);

      spanLike.innerText = 'Likes: ' + 0;
      p3.appendChild(spanLike);
      like.innerHTML = '<img src="https://cdn4.iconfinder.com/data/icons/evil-icons-user-interface/64/like-48.png" alt="">';
      like.addEventListener('click', function(){
        commentPrototyp.likePlus.call(obj);
        spanLike.innerText = '';
        spanLike.innerText = 'Likes: ' + obj.likes;
      });
      p3.appendChild(like);

      message.appendChild(p1);
      message.appendChild(p2);
      message.appendChild(p3);

      message.classList.add('message');
      CommentsFeed.appendChild(message);

    });
  }

  release(CommentsArray);

});