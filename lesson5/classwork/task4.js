/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Метод charCodeAt() возвращает указанный символ из строки.  
    //'b'.charAt()

    "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾=


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)
*/

document.addEventListener('DOMContentLoaded', function(){

  let encryptWord = '',
      decryptWord = '',
      change,
      max = 1103,
      min = 1072;

//шифратор
  function encryptCesar(shift, word){
      let chars = [],
          arr = word.split('');

      encryptWord = '';
      change = 0;
      

      arr.forEach(function(item, index){
        let code = item.charCodeAt();
        
        code += shift;


        if (code > max){
            code = code - max + min - 1;
            chars[index] = code;
          }else {
            chars[index] = code;
            }
      });

          chars.forEach(function(item){
            let char = String.fromCharCode(item);
            encryptWord += char;
          });

        change = shift;

        console.log('Зашифрованное слово "' + word + '" со сдвигом на ' + shift + ' букв - ' + encryptWord);
        decryptCesar(); //вызов дешифратора
  }

//дешифратор
  function decryptCesar(){
      let chars = [],
      arr = encryptWord.split('');
     
      decryptWord = '';

      arr.forEach(function(item, index){
        let code = item.charCodeAt();
        
        code -= change;

        if (code < min){
            code = code + max - min + 1;
            chars[index] = code;
          }else {
            chars[index] = code;
            }
      });
        console.log(chars)
       
        chars.forEach(function(item){
            let char = String.fromCharCode(item);
            decryptWord += char;
          });

        console.log('Применив дешифратор, получаем слово: "' + decryptWord + '".');
  }

encryptCesar(1, 'зая');

encryptCesar(11, 'программирование');

Shift5 = encryptCesar.bind(null, 5);
Shift3 = encryptCesar.bind(null, 3);

Shift5('слово');
Shift3('строка');

})