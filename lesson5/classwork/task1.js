/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
var x = 100;
	Train = {
	name: 'Elektrichka',
	speed: 70,
	passangers: 500,
	run(){
		console.log('Train ' + this.name + ' carries ' + this.passangers + ' passangers with speed ' + this.speed + ' km/h');
	},
	stop(){
		this.speed = 0;
		console.log('Поезд ' + this.name + ' остановился. Скорость ' + this.speed + ' км/ч');
	},
	pickUp(){
		this.passangers += x;
		console.log(this.passangers);
	}
}
Train.run();
Train.stop();
Train.pickUp();
