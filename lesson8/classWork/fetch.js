/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

document.addEventListener('DOMContentLoaded', function(){

  let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
  
  const ConvertToJSON = data => data.json();


 function DataHandler( json ){
    
        let elem = document.createElement('div');
        elem.innerHTML = `
        <div>
          <b>Name: </b>${json.name}, </br>
          <b>Age: </b>${json.age}, </br>
          <b>Adress: </b>${json.address}, </br>
          <b>Phone: </b>${json.phone}, </br>
          <b>Gender: </b>${json.gender}, </br>
          <b>Friends: </b> ${json.friends[0].name}, ${json.friends[1].name}, ${json.friends[2].name}, ${json.friends[3].name}, ${json.friends[4].name}, ${json.friends[5].name}
        </div>
        </div>
        `
      document.body.appendChild(elem);
     
  }

  fetch(url)
  .then( ConvertToJSON )
  .then( response => {
    let random = Math.floor(Math.random() * 9);
    return response[random];
  })
  .then( response => {

    let url = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';
    return fetch(url)
    .then( ConvertToJSON )
    .then( response2 => {

      let getFriends = {
        friends: response2[0].friends
      };

      user  = {
        name: response.name,
        age: response.age,
        address: response.address,
        company: response.company,
        phone: response.phone,
        gender: response.gender,
        friends: {
          ...getFriends.friends
        }
      }
      return user;
    })
  })
  .then( DataHandler )


})